<?php

namespace App\Repository;

use App\Entity\Pavilions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pavilions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pavilions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pavilions[]    findAll()
 * @method Pavilions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PavilionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pavilions::class);
    }

    // /**
    //  * @return Pavilions[] Returns an array of Pavilions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Pavilions
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
