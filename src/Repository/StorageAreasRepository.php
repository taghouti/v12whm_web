<?php

namespace App\Repository;

use App\Entity\StorageAreas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StorageAreas|null find($id, $lockMode = null, $lockVersion = null)
 * @method StorageAreas|null findOneBy(array $criteria, array $orderBy = null)
 * @method StorageAreas[]    findAll()
 * @method StorageAreas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StorageAreasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StorageAreas::class);
    }

    // /**
    //  * @return StorageAreas[] Returns an array of StorageAreas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StorageAreas
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
