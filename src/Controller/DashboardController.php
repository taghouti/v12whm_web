<?php

namespace App\Controller;

use App\Entity\Banner;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends EasyAdminController
{
    /**
     * @Route("/admin/dashboard", name="dashboard")
     */
    public function index()
    {
        $user_repository = $this->getDoctrine()->getRepository(User::class);
        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController', 'users_count' => count($user_repository->findAll())
        ]);
    }
}