<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200728155120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE providers (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone INT NOT NULL, map_x VARCHAR(255) DEFAULT NULL, map_y VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE storage_areas (id INT AUTO_INCREMENT NOT NULL, pavilion_id INT NOT NULL, location VARCHAR(255) NOT NULL, INDEX IDX_35647B7CDDEB8913 (pavilion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, name VARCHAR(255) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, disabled TINYINT(1) DEFAULT \'0\' NOT NULL, username VARCHAR(180) NOT NULL, phone VARCHAR(255) NOT NULL, gender VARCHAR(255) DEFAULT NULL, reset_password VARCHAR(255) DEFAULT NULL, is_admin TINYINT(1) DEFAULT \'0\' NOT NULL, is_clients_manager TINYINT(1) DEFAULT \'0\' NOT NULL, is_providers_manager TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, storage_area_id INT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, quantity INT NOT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_B3BA5A5ACA3373FE (storage_area_id), INDEX IDX_B3BA5A5A12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feeds (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, provider_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, receiption_date DATETIME NOT NULL, confirmed TINYINT(1) NOT NULL, INDEX IDX_5A29F52FA76ED395 (user_id), INDEX IDX_5A29F52FA53A8AA (provider_id), INDEX IDX_5A29F52F4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, client_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, completed TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_E52FFDEEA76ED395 (user_id), INDEX IDX_E52FFDEE19EB6921 (client_id), INDEX IDX_E52FFDEE4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pavilions (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone INT NOT NULL, map_x VARCHAR(255) DEFAULT NULL, map_y VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE storage_areas ADD CONSTRAINT FK_35647B7CDDEB8913 FOREIGN KEY (pavilion_id) REFERENCES pavilions (id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5ACA3373FE FOREIGN KEY (storage_area_id) REFERENCES storage_areas (id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE feeds ADD CONSTRAINT FK_5A29F52FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE feeds ADD CONSTRAINT FK_5A29F52FA53A8AA FOREIGN KEY (provider_id) REFERENCES providers (id)');
        $this->addSql('ALTER TABLE feeds ADD CONSTRAINT FK_5A29F52F4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE feeds DROP FOREIGN KEY FK_5A29F52FA53A8AA');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5ACA3373FE');
        $this->addSql('ALTER TABLE feeds DROP FOREIGN KEY FK_5A29F52FA76ED395');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEA76ED395');
        $this->addSql('ALTER TABLE feeds DROP FOREIGN KEY FK_5A29F52F4584665A');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE4584665A');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5A12469DE2');
        $this->addSql('ALTER TABLE storage_areas DROP FOREIGN KEY FK_35647B7CDDEB8913');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE19EB6921');
        $this->addSql('DROP TABLE providers');
        $this->addSql('DROP TABLE storage_areas');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE feeds');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE pavilions');
        $this->addSql('DROP TABLE clients');
    }
}
