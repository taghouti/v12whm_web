<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StorageAreasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=StorageAreasRepository::class)
 */
class StorageAreas
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Pavilions::class, inversedBy="storageAreas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pavilion;

    /**
     * @ORM\OneToMany(targetEntity=Products::class, mappedBy="storage_area")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPavilion(): ?Pavilions
    {
        return $this->pavilion;
    }

    public function setPavilion(?Pavilions $pavilion): self
    {
        $this->pavilion = $pavilion;

        return $this;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setStorageArea($this);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getStorageArea() === $this) {
                $product->setStorageArea(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getPavilion()->getName() . " " . $this->getLocation();
    }
}
