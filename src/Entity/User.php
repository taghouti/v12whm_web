<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(
 *     normalizationContext={"groups"={"user:read"}},
 *     denormalizationContext={"groups"={"user:write"}},
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email()
     * @Assert\NotBlank
     * @Groups({"user:read", "user:write"})
     *
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Groups({"user:read", "user:write"})
     *
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user:read", "user:write"})
     */
    private $roles = ['ROLE_USER'];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     * @Assert\Length(min=8, minMessage="Le mot de passe doit être composé d'au moins 8 caractères")
     * @Groups({"user:write"})
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Assert\NotBlank
     * @Groups({"user:read", "user:write"})
     */
    private $disabled = 0;


    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\NotBlank
     * @Groups({"user:read", "user:write"})
     *
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Groups({"user:read", "user:write"})
     *
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @Groups({"user:read", "user:write"})
     *
     */
    private $gender;

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        if (!empty($password)) {
            $this->password = $password;
        }
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        return "";
    }

    public function getPlainPassword(): string
    {
        return $this->getPassword();
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function isDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;
        return $this;
    }

    public function __toString(): string
    {
        return (string)$this->username;
    }

    /**
     * @var string reset password token
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:write"})
     */
    protected $reset_password;

    /**
     * @ORM\OneToMany(targetEntity=Orders::class, mappedBy="user")
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity=Feeds::class, mappedBy="user")
     */
    private $feeds;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $is_admin;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $is_clients_manager;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $is_providers_manager;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->feeds = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getResetPassword(): ?string
    {
        return $this->reset_password;
    }

    /**
     * @param string $reset_password
     */
    public function setResetPassword(string $reset_password): void
    {
        $this->reset_password = $reset_password;
    }

    /**
     * @return Collection|Orders[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Orders $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUser($this);
        }

        return $this;
    }

    public function removeOrder(Orders $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getUser() === $this) {
                $order->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Feeds[]
     */
    public function getFeeds(): Collection
    {
        return $this->feeds;
    }

    public function addFeed(Feeds $feed): self
    {
        if (!$this->feeds->contains($feed)) {
            $this->feeds[] = $feed;
            $feed->setUser($this);
        }

        return $this;
    }

    public function removeFeed(Feeds $feed): self
    {
        if ($this->feeds->contains($feed)) {
            $this->feeds->removeElement($feed);
            // set the owning side to null (unless already changed)
            if ($feed->getUser() === $this) {
                $feed->setUser(null);
            }
        }

        return $this;
    }

    public function getIsAdmin(): ?bool
    {
        return $this->is_admin;
    }

    public function setIsAdmin(bool $is_admin): self
    {
        $this->is_admin = $is_admin;

        return $this;
    }

    public function getIsClientsManager(): ?bool
    {
        return $this->is_clients_manager;
    }

    public function setIsClientsManager(bool $is_clients_manager): self
    {
        $this->is_clients_manager = $is_clients_manager;

        return $this;
    }

    public function getIsProvidersManager(): ?bool
    {
        return $this->is_providers_manager;
    }

    public function setIsProvidersManager(bool $is_providers_manager): self
    {
        $this->is_providers_manager = $is_providers_manager;

        return $this;
    }
}
