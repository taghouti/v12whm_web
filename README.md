### V12WHM

##### Prepare the environment:

> N.B: all commands should be run under the project folder, commands
> where run under a machine running ubuntu 20.04

```Bash
yes|cp -f env-example .env
```
>   N.B: modify the line that start with `DATABASE_URL=` in `.env` using your
> own mysql server credentials, in my case i used docker to run a 
> mysql server 5.7, the command i used to start the server was `docker run --name mysql-5.7 -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -d mysql:5.7.30
> `

than you need to install php packages needed using:
```bash
composer install
```

#### Migrate the database using:

```bash
symfony console doctrine:database:create
symfony console doctrine:migrations:migrate
symfony console doctrine:fixture:load
```

#### Run the webapp

```bash
symfony proxy:start
symfony proxy:domain:attach v12whm
symfony serve -d
```

#### Generate JWT authentication key

```bash
mkdir -p config/jwt
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

#### JWT Token

In some cases, to make the token working under apache, you should some lines to your virtual host
```
    RewriteCond %{HTTP:Authorization} ^(.*)
    RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
```


> N.B: when running the above commands, you will be
> requested to enter ` PEM pass phrase:`, just remember it
> because you will need to enter it `.env` file in the line
> starting with `JWT_PASSPHRASE=`, in my case i used to `v12whm`
> passphrase when running the above command that's way i'm using
> that word in `JWT_PASSPHRASE=` line ine `.env` file 
